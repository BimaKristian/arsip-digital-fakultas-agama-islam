<?php include 'header.php'; ?>

<div class="breadcome-area">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="breadcome-list">
                    <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <div class="breadcome-heading">
                                <h4 style="margin-bottom: 0px">Data Arsip</h4>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <ul class="breadcome-menu" style="padding-top: 0px">
                                <li><a href="#">Home</a> <span class="bread-slash">/</span></li>
                                <li><span class="bread-blod">Arsip</span></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container-fluid">


    <div class="panel">

        <div class="panel-body">

            <form method="get" action="">

                <div class="row">

                    <div class="col-lg-3">
                        <div class="form-group">
                            <label>Filter Kategori</label>
                            <select class="form-control" name="kategori">
                                <option value="">Pilih kategori</option>
                                <?php 
                                $kategori = mysqli_query($koneksi,"SELECT * FROM kategori");
                                while($k = mysqli_fetch_array($kategori)){
                                    ?>
                                    <option <?php if(isset($_GET['kategori'])){if($_GET['kategori'] == $k['id']){echo "selected='selected'";}} ?> value="<?php echo $k['id']; ?>"><?php echo $k['kode_kategori']; ?>

                                </option>
                                <?php 
                            }
                            ?>
                        </select>
                    </div>

                </div>

                <div class="col-lg-4">
                    <br>
                    <input type="submit" class="btn btn-success" value="Tampilkan">
                </div>

            </div>
        </form>

    </div>

</div>

<div class="panel">

    <div class="panel-heading">
        <h3 class="panel-title" align="center">Data arsip</h3>
    </div>
    
    <div class="panel-body">
        <div class="pull-right">
            <a href="cetak.php" class="btn btn-success"><i class="fa fa-print" aria-hidden="true">Cetak</i></a>
        </div>
        <br>
        <br>
        <br>
        <table id="table" class="table table-bordered table-striped table-hover table-datatable">
         <thead>
            <tr>
                <th width="1%">No</th>
                <th>Arsip</th>
                <th>Kategori</th>
                <th>Kode Kategori</th>
                <th>Tanggal Arsip</th>
                <th class="text-center" width="20%">Aksi</th>
            </tr>
        </thead>
        <tbody>
            <?php 

            $no = 1;
            if(isset($_GET['kategori'])){
                $kategori = $_GET['kategori'];
                $arsip = mysqli_query($koneksi,"SELECT * FROM data_arsip,kategori WHERE kategori_arsip=id and kategori_arsip='$kategori' ORDER BY tgl_arsip DESC");
            }else{
                $arsip = mysqli_query($koneksi,"SELECT * FROM data_arsip,kategori WHERE kategori_arsip=id ORDER BY tgl_arsip desc");
            }
            while($p = mysqli_fetch_array($arsip)){
                ?>
                <tr> 
                    <td><?php echo $no++; ?></td>
                    <td>
                        <b>KODE</b> : <?php echo $p['kode_arsip'] ?><br>
                        <b>Nama</b> : <?php echo $p['nama_arsip'] ?><br>
                        <b>Jenis</b> : <?php echo $p['jenis_file_arsip'] ?><br>
                    </td>
                    <td><?php echo $p['nama_kategori'] ?></td>
                    <td><?php echo $p['kode_kategori'] ?></td>
                    <td><?php echo date('d-m-Y',strtotime($p['tgl_arsip'])); ?></td>
                    <td class="text-center">
                        <div class="btn-group">
                            <a target="blank" class="btn btn-default" href="arsip_download.php?id_arsip=<?php echo $p['id_arsip']; ?>"><i class="fa fa-download"></i></a>
                            <a class="btn btn-default" target="blank"href="arsip_preview.php?id_arsip=<?php echo $p['id_arsip']; ?>"><i class="fa fa-eye"></i></a>
                        </div>
                    </td>
                </tr>
                <?php 
            }
            ?>
        </tbody>
    </table>


</div>

</div>
</div>


<?php include 'footer.php'; ?>