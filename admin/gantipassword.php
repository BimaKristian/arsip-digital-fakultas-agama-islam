<?php include 'header.php'; ?>

<div class="breadcome-area">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="breadcome-list">
                    <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <div class="breadcome-heading">
                                <h4 style="margin-bottom: 0px">Ganti Password</h4>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <ul class="breadcome-menu" style="padding-top: 0px">
                                <li><a href="index.php">Home</a> <span class="bread-slash">/</span></li>
                                <li><span class="bread-blod">Ganti Password</span></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="product-sales-area mg-tb-30">
    <div class="container-fluid">
        <div class="row">

            <div class="col-lg-3">

                <?php 
                $id = $_SESSION['id'];
                $saya = mysqli_query($koneksi,"select * from user where level='admin'");
                $s = mysqli_fetch_assoc($saya);
                ?>


            </div>

            <div class="col-lg-6">

                <?php 
                if(isset($_GET['alert'])){
                    if($_GET['alert'] == "sukses"){

                    }
                }
                ?>

                <div class="panel">
                    <div class="panel-heading">
                        <h4 align="center">Ganti Password</h4>
                    </div>
                    <div class="panel-body">

                        <form action="password_act.php" method="post" enctype="multipart/form-data">

                           <?php 
                           if(isset($_GET['alert'])){
                            if($_GET['alert'] == "sukses"){
                                echo "<div class='alert alert-success'>Password anda berhasil dirubah!</div>";
                            }
                        }
                        ?>

                        <form action="gantipassword_act.php" method="post">
                            <div class="form-group">
                                <label>Masukkan Password Baru</label>
                                <input type="password" class="form-control" placeholder="Masukkan Password Baru .." name="password" required="required" min="5">
                            </div>
                            <div class="form-group">
                                <p align="center"><input type="submit" class="btn btn-success" value="Simpan"></p>
                            </div>
                            


                        </form>



                    </div>

                </div>
            </div>

        </div>
    </div>
</div>



    <?php include 'footer.php'; ?>