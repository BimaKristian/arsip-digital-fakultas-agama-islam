<?php include 'header.php'; ?>

<div class="breadcome-area">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="breadcome-list">
                    <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <div class="breadcome-heading">
                                <h4 style="margin-bottom: 0px">Data Riwayat</h4>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <ul class="breadcome-menu" style="padding-top: 0px">
                                <li><a href="#">Home</a> <span class="bread-slash">/</span></li>
                                <li><span class="bread-blod">Riwayat</span></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container-fluid">
    <div class="panel panel">

        <div class="panel-heading">
            <h3 class="panel-title" align="center">Data Riwayat Unduhan Arsip</h3>
        </div>
        <div class="panel-body">


            <table id="table" class="table table-bordered table-striped table-hover table-datatable">
                <thead>
                    <tr>
                        <th width="1px">No</th>
                        <th>Waktu Unduh</th>
                        <th>Pengguna</th>
                        <th>Arsip yang diunduh</th>
                        <th>Kode Arsip</th>
                    </tr>
                </thead>
                <tbody>
                    <?php 
                    include '../koneksi.php';
                    $no = 1;
                    $saya = $_SESSION['id'];
                    $arsip = mysqli_query($koneksi,"SELECT nama,nama_arsip,`kode_arsip`,waktu FROM `data_arsip`,USER,`riwayat_unduh` 
WHERE `data_arsip`.`id_arsip`= `riwayat_unduh`.`id_arsip` AND user.`id`=riwayat_unduh.`id_pengguna` ORDER BY waktu DESC");
                    while($p = mysqli_fetch_array($arsip)){
                        ?>
                        <tr>
                            <td><?php echo $no++; ?></td>
                            <td><?php echo date('d-m-Y || H:i:s',strtotime($p['waktu'])) ?></td>
                            <td><?php echo $p['nama'] ?></td>
                            <td><?php echo $p['nama_arsip'] ?></td>
                            <td><?php echo $p['kode_arsip'] ?></td>
                        </tr>
                        <?php 
                    }
                    ?>
                </tbody>
            </table>


        </div>

    </div>
</div>


<?php include 'footer.php'; ?>