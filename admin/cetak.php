<?php
include '../koneksi.php';
require_once("..//dompdf/autoload.inc.php");
use Dompdf\Dompdf;
$dompdf = new Dompdf();
$query = mysqli_query($koneksi,"SELECT * FROM data_arsip,kategori WHERE kategori_arsip=id ORDER BY tgl_arsip desc");
$html = '<center><h3>Laporan Arsip<p> <h3>Fakultas Agama Islam </p></h3></h3></center><hr/><br/>';
$html .= '<table border="1" width="100%">
 <tr>
 <th>No</th>
 <th>Nama Arsip</th>
 <th>Kode Arsip</th>
 <th>Kategori Arsip</th>
 <th>Tanggal Arsip </th>
 </tr>';
$no = 1;
while($row = mysqli_fetch_array($query))
{
 $html .= "<tr>
 <td>".$no."</td>
 <td>".$row['nama_arsip']."</td>
 <td>".$row['kode_arsip']."</td>
 <td>".$row['kode_kategori']."</td>
 <td>".$row['tgl_arsip']."</td>
 </tr>";
 $no++;
}
$html .= "</html>";
$dompdf->loadHtml($html);
// Setting ukuran dan orientasi kertas
$dompdf->setPaper('A4', 'potrait');
// Rendering dari HTML Ke PDF
$dompdf->render();
// Melakukan output file Pdf
$dompdf->stream('laporan_arsip.pdf');
?>