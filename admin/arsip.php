<?php include 'header.php'; ?>

<div class="breadcome-area">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="breadcome-list">
                    <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <div class="breadcome-heading">
                                <h4 style="margin-bottom: 0px">Data Arsip</h4>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <ul class="breadcome-menu" style="padding-top: 0px">
                                <li><a href="index.php">Home</a> <span class="bread-slash">/</span></li>
                                <li><span class="bread-blod"><a href="arsip.php">Arsip</a></span></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container-fluid">
    <div class="panel panel">

        <div class="panel-heading">
            <h3 class="panel-title" align="center">Data Arsip</h3>
        </div>
        <div class="panel-body">

            <div class="pull-right">
                <a href="arsip_tambah.php" class="btn btn-primary"><i class="fa fa-file-archive-o"></i> Tambah Arsip</a>
            </div>
            <br>
            <br>
            <br>
            <div class="pull-right">
                <a href="cetak.php" class="btn btn-success"><i class="fa fa-print"></i> Cetak</a>
            </div>
            <br>
            <br>
            <br>

            <table id="table" class="table table-bordered table-striped table-hover table-datatable">
                <thead>
                    <tr>
                        <th width="1%">No</th>
                        <th>Arsip</th>
                        <th>Kategori</th>
                        <th>Kode Kategori</th>
                        <th>Tanggal Arsip</th>
                        <th class="text-center" width="20%">OPSI</th>
                    </tr>
                </thead>
                <tbody>
                    <?php 
                    include '../koneksi.php';
                    $no = 1;
                    $arsip = mysqli_query($koneksi,"SELECT * FROM data_arsip,kategori WHERE kategori_arsip=id ORDER BY tgl_arsip desc");
                    while($p = mysqli_fetch_array($arsip)){
                        ?>
                        <tr>
                            <td><?php echo $no++; ?></td>
                    <td>
                        <b>KODE</b> : <?php echo $p['kode_arsip'] ?><br>
                        <b>Nama</b> : <?php echo $p['nama_arsip'] ?><br>
                        <b>Jenis</b> : <?php echo $p['jenis_file_arsip'] ?><br>
                    </td>
                    <td><?php echo $p['nama_kategori'] ?></td>
                    <td><?php echo $p['kode_kategori'] ?></td>
                    <td><?php echo date('d-m-Y',strtotime($p['tgl_arsip'])); ?></td>
                            <td class="text-center">


                                <div class="btn-group">
                                    <a  target="blank"class="btn btn-default" href="arsip_download.php?id_arsip=<?php echo $p['id_arsip']; ?>"><i class="fa fa-download"></i></a>
                                    <a target="blank" href="arsip_preview.php?id_arsip=<?php echo $p['id_arsip']; ?>" class="btn btn-default"><i class="fa fa-eye"></i>
                                    </a>
                                    <a href="arsip_edit.php?id_arsip=<?php echo $p['id_arsip']; ?>" class="btn btn-default"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                                    </a>
                                    <a href="arsip_hapus.php?id_arsip=<?php echo $p['id_arsip']; ?>" class="btn btn-default"> <i class="fa fa-trash"></i>
                                    </a>
                                </div>
                            </td>
                        </tr>
                        <?php 
                    }
                    ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
<?php include 'footer.php'; ?>