<?php include 'header.php'; ?>

<div class="breadcome-area">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="breadcome-list">
                    <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <div class="breadcome-heading">
                                <h4 style="margin-bottom: 0px">Edit Arsip</h4>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <ul class="breadcome-menu" style="padding-top: 0px">
                                <li><a href="#">Home</a> <span class="bread-slash">/</span></li>
                                <li><span class="bread-blod">Arsip</span></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container-fluid">


    <div class="row">
        <div class="col-lg-6 col-lg-offset-3">
            <div class="panel panel">

                <div class="panel-heading">
                    <h3 class="panel-title">Edit Arsip</h3>
                </div>
                <div class="panel-body">

                    <div class="pull-right">            
                        <a href="arsip.php" class="btn btn-sm btn-primary"><i class="fa fa-arrow-left"></i> Kembali</a>
                    </div>

                    <br>
                    <br>


                    <?php 
                    $id = $_GET['id_arsip'];              
                    $data = mysqli_query($koneksi, "select * from data_arsip where id_arsip='$id'");
                    while($d = mysqli_fetch_array($data)){
                        ?>

                        <form method="post" action="arsip_update.php" enctype="multipart/form-data">

                            <div class="form-group">
                                <label>Kode Arsip</label>
                                <input type="hidden" name="id_arsip" value="<?php echo $d['id_arsip']; ?>">
                                <input type="text" class="form-control" name="kode_arsip" required="required" value="<?php echo $d['kode_arsip']; ?>">
                            </div>

                            <div class="form-group">
                                <label>Nama Arsip</label>
                                <input type="text" class="form-control" name="nama_arsip" required="required" value="<?php echo $d['nama_arsip']; ?>">
                            </div>

                            <div class="form-group">
                                <label>Kategori</label>
                                <select class="form-control" name="kategori_arsip" required="required">
                                    <option value="">Pilih kategori</option>
                                    <?php 
                                    $kategori = mysqli_query($koneksi,"SELECT * FROM kategori");
                                    while($k = mysqli_fetch_array($kategori)){
                                        ?>
                                        <option <?php if($k['id'] == $d['kategori_arsip']){echo "selected='selected'";} ?> value="<?php echo $k['id']; ?>"><?php echo $k['kode_kategori']; ?></option>
                                        <?php 
                                    }
                                    ?>
                                </select>
                            </div>

                            <div class="form-group">
                                <label>Tanggal Arsip</label>
                                <input type="date" class="form-control" name="tanggal_arsip" required="required" value="<?php echo $d['tgl_arsip']; ?>">
                            </div>

                            <div class="form-group">
                                <label>File</label>
                                <input type="file" name="file">
                                <small>Kosongkan jika tidak ingin mengubah file</small>
                            </div>

                            
                            <div class="form-group">
                                <p align="center">
                                    <input type="submit" class="btn btn-primary" value="Edit"></p>
                                </div>

                            </form>

                            <?php 
                        }
                        ?>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php include 'footer.php'; ?>