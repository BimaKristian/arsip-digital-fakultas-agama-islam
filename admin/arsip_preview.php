<?php include 'header.php'; ?>

<div class="breadcome-area">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="breadcome-list">
                    <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <div class="breadcome-heading">
                                <h4 style="margin-bottom: 0px">Preview Arsip</h4>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <ul class="breadcome-menu" style="padding-top: 0px">
                                <li><a href="#">Home</a> <span class="bread-slash">/</span></li>
                                <li><span class="bread-blod">Arsip</span></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container-fluid">


    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel">

                <div class="panel-heading">
                    <h3 class="panel-title">Preview Arsip</h3>
                </div>
                <div class="panel-body">

                    <a href="arsip.php" class="btn btn-sm btn-primary"><i class="fa fa-arrow-left"></i> Kembali</a>

                    <br>
                    <br>

                    <?php 
                    $id = $_GET['id_arsip'];  
                    $data = mysqli_query($koneksi,"SELECT * FROM data_arsip,kategori WHERE kategori_arsip=id and id_arsip='$id'");
                    while($d = mysqli_fetch_array($data)){
                        ?>

                        <div class="row">
                            <div class="col-lg-4">

                                <table class="table">
                                    <tr>
                                        <th>Kode Arsip</th>
                                        <td><?php echo $d['kode_arsip']; ?></td>
                                    </tr>
                                    <tr>
                                        <tr>
                                            <th>Nama Arsip</th>
                                            <td><?php echo $d['nama_arsip']; ?></td>
                                        </tr>
                                        <tr>
                                            <th>Kategori</th>
                                            <td><?php echo $d['nama_kategori']; ?></td>
                                        </tr>
                                        <tr>
                                            <th>Tanggal Arsip</th>
                                            <td><?php echo date('d-m-Y',strtotime($d['tgl_arsip'])); ?></td>
                                        </tr>
                                        <th>Jenis File</th>
                                        <td><?php echo $d['jenis_file_arsip']; ?></td>
                                    </tr>
                                    <tr>
                                        <th>Download File</th>
                                        <td><a href="arsip_download.php?id_arsip=<?php echo $d['id_arsip']; ?>"style="color: blue">Klik Disini</a></td>
                                    </tr>
                                    
                                </table>

                            </div>
                            <div class="col-lg-8">

                                <?php 
                                if($d['jenis_file_arsip'] == "png" || $d['jenis_file_arsip'] == "jpg" || $d['jenis_file_arsip'] == "gif" || $d['jenis_file_arsip'] == "jpeg"){
                                    ?>
                                    <img src="../arsip/<?php echo $d['file_arsip']; ?>">
                                    
                                    <?php
                                }else{
                                    ?>
                                    <p>Preview tidak tersedia, silahkan <a href="arsip_download.php?id_arsip=<?php echo $d['id_arsip']; ?>"style="color: blue">Download di sini.</a></p>.
                                    <?php
                                }
                                ?>

                            </div>
                        </div>
                        <?php 
                    }
                    ?>

                </div>
            </div>
        </div>
    </div>


</div>



<?php include 'footer.php'; ?>