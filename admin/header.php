<!doctype html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Admin - Arsip Digital Fakultas Agama Islam</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" type="image/x-icon" href="img/favicon.ico">
    <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,700,900" rel="stylesheet">
    <link rel="stylesheet" href="../assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="../assets/css/font-awesome.min.css">
    <link rel="stylesheet" href="../assets/css/owl.carousel.css">
    <link rel="stylesheet" href="../assets/css/owl.theme.css">
    <link rel="stylesheet" href="../assets/css/owl.transitions.css">
    <link rel="stylesheet" href="../assets/css/animate.css">
    <link rel="stylesheet" href="../assets/css/normalize.css">
    <link rel="stylesheet" href="../assets/css/meanmenu.min.css">
    <link rel="stylesheet" href="../assets/css/main.css">
    <link rel="stylesheet" href="../assets/css/educate-custon-icon.css">
    <link rel="stylesheet" href="../assets/css/morrisjs/morris.css">
    <link rel="stylesheet" href="../assets/css/scrollbar/jquery.mCustomScrollbar.min.css">
    <link rel="stylesheet" href="../assets/css/metisMenu/metisMenu.min.css">
    <link rel="stylesheet" href="../assets/css/metisMenu/metisMenu-vertical.css">
    <link rel="stylesheet" href="../assets/css/calendar/fullcalendar.min.css">
    <link rel="stylesheet" href="../assets/css/calendar/fullcalendar.print.min.css">
    <link rel="stylesheet" href="../assets/style.css">
    <link rel="stylesheet" href="../assets/css/responsive.css">
    <link rel="stylesheet" href="../assets/css/style.css">

    <link rel="stylesheet" type="text/css" href="../assets/js/DataTables/datatables.css">

    <script src="../assets/js/vendor/modernizr-2.8.3.min.js"></script>

    <?php 
    include '../koneksi.php';
    session_start();
    if($_SESSION['status'] != "admin_login"){
        header("location:../login.php?alert=belum_login");
    }
    ?>
</head>
<body>
    <div class="left-sidebar-pro">
        <nav id="sidebar" class="">
            <div class="left-custom-menu-adp-wrap comment-scrollbar">
                <nav class="sidebar-nav left-sidebar-menu-pro" style="margin-top: 30px">
                    <ul class="metismenu" id="menu1">
                        <li class="active">
                            <a href="index.php">
                                <span class="fa fa-home"></span>
                                <span class="mini-click-non">Dashboard</span>
                            </a>
                        </li>

                        <li>
                            <a href="kategori.php" aria-expanded="false"><span class="fa fa-book" aria-hidden="true"></span> <span class="mini-click-non">Data Kategori</span></a>
                        </li>

                        <li>
                            <a href="arsip.php" aria-expanded="false"><span class="fa fa-file" aria-hidden="true"></span> <span class="mini-click-non">Data Arsip</span></a>
                        </li>

                        <li>
                            <a href="user.php" aria-expanded="false"><span class="fa fa-users" aria-hidden="true"></span> <span class="mini-click-non">Data Pengguna</span></a>
                        </li>

                        <li>
                            <a href="riwayat.php" aria-expanded="false"><span class="fa fa-history" aria-hidden="true"></span> <span class="mini-click-non">Riwayat Unduh</span></a>
                        </li>

                    </ul>
                </nav>
            </div>
        </nav>
    </div>
    <!-- End Left menu area -->
    <!-- Start Welcome area -->
    <div class="all-content-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="logo-pro">
                        <a href="index.php"><img class="main-logo" src="../assets/img/logo/logo.png" alt="" /></a>
                    </div>
                </div>
            </div>
        </div>
        <div class="header-advance-area">
            <div class="header-top-area">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="header-top-wraper">
                                <div class="row">
                                    <div class="col-lg-1 col-md-0 col-sm-12 col-xs-12">
                                        <div class="menu-switcher-pro">
                                            <button type="button" id="sidebarCollapse" class="btn bar-button-pro header-drl-controller-btn btn-info navbar-btn">
                                                <i class="educate-icon educate-nav"></i>
                                            </button>
                                        </div>
                                    </div>
                                    <div class="col-lg-5 col-md-6 col-sm-12 col-xs-12">
                                        <div class="header-top-menu tabl-d-n">
                                            <ul class="nav navbar-nav mai-top-nav">
                                                <li class="nav-item"><a href="index.php" class="nav-link">Arsip Digital Fakultas Agama Islam</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <div class="header-right-info">
                                            <ul class="nav navbar-nav mai-top-nav header-right-menu">

                                                <li class="nav-item">
                                                    <a href="#" data-toggle="dropdown" role="button" aria-expanded="false" class="nav-link dropdown-toggle"><i class="educate-icon educate-bell" aria-hidden="true"></i><span class="indicator-nt"></span></a>
                                                    <div role="menu" class="notification-author dropdown-menu animated zoomIn">
                                                        <div class="notification-single-top">
                                                            <h1>Riwayat unduh terakhir</h1>
                                                        </div>
                                                        <ul class="notification-menu">
                                                            <?php 
                                                            $arsip = mysqli_query($koneksi,"SELECT nama,nama_arsip,waktu FROM `data_arsip`,USER,`riwayat_unduh` WHERE `data_arsip`.`id_arsip`= `riwayat_unduh`.`id_arsip` AND user.`id`=riwayat_unduh.`id_pengguna` order by waktu desc");
                                                            while($p = mysqli_fetch_array($arsip)){
                                                                ?>
                                                                <li>
                                                                    <a href="riwayat.php">
                                                                        <div class="notification-content">
                                                                         <p>
                                                                            <small><i><?php echo date('d-m-Y || H:i:s',strtotime($p['waktu'])) ?></i></small>
                                                                            <br>
                                                                            <b><?php echo $p['nama'] ?></b> mengunduh <b><?php echo $p['nama_arsip'] ?></b>.
                                                                        </p>
                                                                    </div>
                                                                </a>
                                                                <hr>
                                                            </li>
                                                            <?php 
                                                        }
                                                        ?>
                                                    </ul>
                                                    <div class="notification-view">
                                                        <a href="riwayat.php">Lihat semua riwayat</a>
                                                    </div>
                                                </div>
                                            </li>

                                            <li class="nav-item">
                                                <a href="#" data-toggle="dropdown" role="button" aria-expanded="false" class="nav-link dropdown-toggle">
                                                    <?php 
                                                    $id= $_SESSION['id'];
                                                    $profil = mysqli_query($koneksi,"select * from user where id='$id'");
                                                    $profil = mysqli_fetch_assoc($profil);
                                                    if($profil['foto'] == ""){ 
                                                      ?>
                                                      <small>foto profil kosong</small>
                                                  <?php }else{ ?>
                                                    <img src="../gambar/admin/<?php echo $profil['foto'] ?>" style="width: 20px;height: 20px">
                                                <?php } ?>
                                                <span class="admin-name"><?php echo $_SESSION['nama']; ?></span>
                                                <i class="fa fa-angle-down edu-icon edu-down-arrow"></i>
                                            </a>
                                            <ul role="menu" class="dropdown-header-top author-log dropdown-menu animated zoomIn">
                                                <li><a href="profil.php"><span class="fa fa-user"></span> Profil Saya</a></li>
                                                <li><a href="gantipassword.php"><span class="fa fa-key"></span> Ganti Password</a></li>
                                                <li><a href="logout.php"><span class="fa fa-sign-out"></span>Log Out</a></li>
                                            </ul>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Mobile Menu start -->
    <div class="mobile-menu-area">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="mobile-menu">
                        <nav id="dropdown">
                            <ul class="mobile-menu-nav">
                                <li class="active">
                                    <a href="index.php">
                                        <span class="fa fa-home"></span>
                                        <span class="mini-click-non">Dashboard</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="kategori.php" aria-expanded="false"><span class="fa fa-book" aria-hidden="true"></span> <span class="mini-click-non">Data Kategori</span></a>
                                </li>

                                <li>
                                    <a href="user.php" aria-expanded="false"><span class="fa fa-users" aria-hidden="true"></span> <span class="mini-click-non">Data Pengguna</span></a>
                                </li>

                                <li>
                                    <a href="arsip.php" aria-expanded="false"><span class="fa fa-file" aria-hidden="true"></span> <span class="mini-click-non">Data Arsip</span></a>
                                </li>

                                <li>
                                    <a href="riwayat.php" aria-expanded="false"><span class="fa fa-history" aria-hidden="true"></span> <span class="mini-click-non">Riwayat Unduh</span></a>
                                </li>

                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>