-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 27, 2020 at 03:28 PM
-- Server version: 10.1.37-MariaDB
-- PHP Version: 7.2.13

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `arsip`
--

-- --------------------------------------------------------

--
-- Table structure for table `data_arsip`
--

CREATE TABLE `data_arsip` (
  `id_arsip` int(11) NOT NULL,
  `kode_arsip` varchar(255) DEFAULT NULL,
  `nama_arsip` varchar(255) DEFAULT NULL,
  `jenis_file_arsip` varchar(255) DEFAULT NULL,
  `kategori_arsip` int(11) DEFAULT NULL,
  `file_arsip` varchar(255) DEFAULT NULL,
  `tgl_arsip` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `data_arsip`
--

INSERT INTO `data_arsip` (`id_arsip`, `kode_arsip`, `nama_arsip`, `jenis_file_arsip`, `kategori_arsip`, `file_arsip`, `tgl_arsip`) VALUES
(3, '2298/B-UIR/9-2020', 'Usulan Kenaikan Pangkat Dosen', 'jpg', 11, '572777367_Usulan kenaikan pangkat.jpg', '2020-08-10'),
(4, '2313/A-UIR/3-2020', 'Undangan', 'jpg', 5, '941515367_undangan.jpg', '2020-08-12'),
(6, '2268/A-UIR/3-2020', 'Undangan', 'jpg', 5, '2128999557_undangan.jpg', '2020-08-10'),
(7, '2177/A-UIR/3-2020', 'Undangan Rapat', 'jpg', 5, '2116791386_undangan rapat.jpg', '2020-07-28'),
(8, '0516/A-UIR/3-2020', 'Undangan', 'jpg', 5, '1564840504_undangan.jpg', '2020-02-03'),
(9, '0134/A-UIR/2-BAAK/2020', 'Fotocoy kurikulum masing-maisng program studi', 'jpg', 4, '2036316164_fotocopy.jpg', '2020-02-04'),
(10, '1616/A-UIR/2-2020', 'Usulan Calon Wakil Dekan Fakultas Agama Islam', 'pdf', 4, '449811232_Usul Wakil Dekan.pdf', '2020-05-20'),
(11, '1635/A-UIR/2-2020', 'Usulan Calon Ketua Prodi Dan Sekretaris Prodi Fakultas Agama Islam', 'pdf', 4, '1315470968_usul prodi.pdf', '2020-05-27'),
(13, '1302/A-UIR/3-PPs/2020', 'Undangan', 'pdf', 5, '1772091734_undangan.pdf', '2020-08-11'),
(14, '0096/A-UIR/2-BAAK/2019', 'Mahasiswa angkatan 2012 yang masih aktif', 'pdf', 4, '792139556_2012.pdf', '2019-01-28'),
(15, '0385/A-UIR/2-2019', 'Laporan Beban Kerja Dosen Semester Ganjil T.A 2018/2019', 'jpg', 4, '1876885114_beban kerja.jpg', '2019-01-31'),
(16, '3423/A-UIR/5-2019', 'Surat Tugas Kegiatan Pengembangan Islam Rahmatan Lilalamin dan Multikultural Mahasiswa PTU', 'jpg', 7, '858537979_surat tugas.jpg', '2019-09-11'),
(17, '191/A-UIR/2-2019', 'Laporan Loka Karya Prodi Pendidikan Bahasa Arab (PBA)', 'jpg', 4, '781637738_loka karya.jpg', '2019-02-11'),
(18, '672/A-UIR/6-2019', 'Permohonan Papan Bunga Ucapan Selamat', 'jpg', 8, '518582644_papan bunga.jpg', '2019-04-10'),
(19, '70/B-UIR/11/STPM/2018', 'Himbauan', 'jpg', 13, '714406975_himbauan.jpg', '2018-04-28'),
(20, '1255/A-UIR/6-2017', 'Surat Perintah Satpam', 'pdf', 8, '1136557469_surat satpam.pdf', '2019-09-20'),
(22, '1182/A-UIR/1-2020', 'Kerjasama Dalam Syiar Sosialisasi Qurban Pegawai Menyambut Idul Adha', 'pdf', 3, '1526338863_qurban.pdf', '2020-06-26'),
(23, '1699/A-UIR/1-2020', 'Salinan Peraturan Rektor Universitas Islam Riau', 'pdf', 3, '1558553859_salinan.pdf', '2020-06-11'),
(24, '1667.A-UIR/1-FAI/2020', 'Pelaksanaan KKN secara Daring', 'jpg', 3, '191473815_kkn.jpg', '2020-06-24'),
(25, '298/A-UIR/1-LPPM/2020', 'File Prosiding', 'pdf', 3, '1743423715_prosiding.pdf', '2020-06-24'),
(26, '1676/A-UIR/1-2020', 'Usulan Jabatan Struktural Kepala Sub. Bagian Ekspedisi dan Agenda', 'pdf', 3, '199951995_jabatan.pdf', '2020-06-03'),
(27, '1666/A-UIR/1/2020', 'Pengiriman Peraturan Rektor UIR Tentang Putus Studi (Drop Out) Mahasiswa', 'pdf', 3, '448340520_do.pdf', '2020-06-03'),
(28, '1582/A-UIR/1-2020', 'Pengiriman Mahasiswa Beasiswa Tahfidz Quran Pemerintah Provinsi Riau', 'jpg', 3, '1086067208_tahfidz.jpg', '2020-05-14'),
(29, '1585/A-UIR/4-2020', 'Pengumuman Beasiswa Tahfidz Quran Pemerintah Provinsi Riau Dinas Pendidikan', 'pdf', 6, '1689139416_tahfidz.pdf', '2020-05-14'),
(30, '1014/A-UIR/1-FAI/2020', 'Pemberitahuan', 'jpg', 3, '1013317256_pemberitahuan.jpg', '2020-05-26'),
(31, '1535/A-UIR/1-2020', 'Data & Dokumen Kegiatan Kemahasiswaan Tahun 2019', 'pdf', 3, '1385446364_dokumen kegiatan.pdf', '2020-05-05'),
(32, '1512/A-UIR/1-2020', 'Pemeringkatan Bidang Kemahasiswaan Tahun 2020', 'pdf', 3, '1926667790_pemeringkatan.pdf', '2020-04-28'),
(33, '935/A-UIR/1-2020', 'Pengunduran Diri Sebagai Sekretaris UPM FAI', 'jpg', 3, '931220989_pengunduran diri.jpg', '2020-05-05'),
(34, '926/A-UIR/1-2020', 'Laporan Pelaksanaan Tugas', 'jpg', 3, '1525809786_laporan.jpg', '2020-03-02'),
(35, '910/A-UIR/1-2020', 'Laporan Data Dosen Baru', 'pdf', 3, '820075686_laporan dosen baru.pdf', '2020-04-28'),
(36, '1368/A-UIR/1-KEU/2020', 'Pemberitahuan', 'jpg', 3, '1252841336_pemberitahuan.jpg', '2020-04-01'),
(37, '1380/A-UIR/1-2020', 'Pemberitahuan Penyemprotan Disinfektan', 'jpg', 3, '212271584_semprot.jpg', '2020-03-31'),
(38, '1130/A-UIR/1-2020', 'Hasil Rapat Koordinasi Akademik', 'pdf', 3, '1289414261_rapat.pdf', '2020-03-10'),
(39, '586/A-UIR/1-FAI', 'Peminjaman Aula FAI', 'jpg', 3, '333528154_aula.jpg', '2020-03-14'),
(40, '556/A-UIR/1-2020', 'Peminjaman Aula FAI ', 'jpg', 3, '2128022035_aula.jpg', '2020-03-13'),
(41, '555-/A-UIR/1-FAI', 'Mohon Izin Memakai Aula FAI-UIR', 'jpg', 3, '2085713985_aula.jpg', '2020-02-13'),
(42, '1175/A-UIR/4-2020', 'Pemberitahuan Libur', 'pdf', 6, '1531587445_libur.pdf', '2020-03-12'),
(43, '516/A-UIR/1', 'Peminjaman Aula', 'jpg', 3, '1532155121_aula.jpg', '2020-03-11'),
(44, '499/A-UIR/1', 'Peminjaman Aula FAI', 'jpg', 3, '2895215_aula.jpg', '2020-03-10'),
(45, '1077/A-UIR/1-2020', 'Pemberitahuan Pemetaan Kerja Kebersihan Universitas Islam Riau', 'pdf', 3, '1665584808_kebersihan.pdf', '2020-03-07'),
(46, '1021/A-UIR/1-2020', 'Pindah Homebase Dosen ', 'jpg', 3, '923729832_homebase.jpg', '2020-03-02'),
(47, '230/A-UIR/1-FAI/2020', 'Surat Edaran', 'jpg', 3, '788741796_edaran.jpg', '2020-02-17'),
(48, '0894/A-UIR/1-2020', 'Pengiriman Bukti Potong A1 PPh 21', 'pdf', 3, '349645307_bukti potongan.pdf', '2020-02-22'),
(49, '239/A-UIR/1-FAI/2020', 'Penawaran Seminar Kitab at-Turats', 'jpg', 3, '357389600_seminar.jpg', '2020-02-18'),
(50, '2314/A-UIR/4-2020', 'Kebijakan Pengurangan Biaya SPP Mahasiswa', 'jpg', 6, '545984368_spp.jpg', '2020-08-13'),
(51, '2332/A-UIR/4-2020', 'Pemberitahuan Pembukaan Sistem Dari Evaluasi Diri (SADAR DIRI)', 'jpg', 6, '1502463689_sadar diri.jpg', '2020-08-13'),
(52, '2316/A-UIR/4-2020', 'Pelaksanaan Wisuda Periode III', 'pdf', 6, '1775272281_wisuda.pdf', '2020-08-13'),
(53, '2263/A-UIR/4-2020', 'Pengisian KRS dan Input Nilai Mata Kuliah Semester Genap 2019/2020', 'jpg', 6, '662509606_krs.jpg', '2020-08-06'),
(54, '1707/A-UIR/4-2020', 'Pemberitahuan Pengusulan Pengangkatan Calon Pegawai Tetap (100%) YLPI Riau', 'pdf', 6, '1551627818_pegawai tetap.pdf', '2020-06-10'),
(55, '1737/A-UIR/4-2020', 'Surat Pernyataan Tidak Melanjutkan Pengisian Dan Pemyusunan LKPS dan LED', 'pdf', 6, '1236593068_1737.pdf', '2020-06-17'),
(56, '1736/A-UIR/4-2020', 'Beban Pokok Dosen Pindah Homebase', 'pdf', 6, '1182027117_1736.pdf', '2020-06-17'),
(57, '1703/A-UIR/4-2020', 'SPMT Semester Genap 2019/2020', 'pdf', 6, '1017111081_1703.pdf', '2020-06-12'),
(58, '1698/A-UIR/4-2020', 'Kegiatan Yudisium dan Temu Ramah Wisudawan Selama Pandemi COVID-19', 'jpg', 6, '1904264728_1698.jpg', '2020-06-10'),
(59, '1084/A-UIR/4-2020', 'Pemberitahuan Pelaksanaan UAS Daring di Masa COVID-19', 'jpg', 6, '1079253549_1084.jpg', '2020-06-10'),
(60, '1673/A-UIR/4-2020', 'Penawaran Program Kegiatan Kompetensi Pendidik Tahun 2020', 'pdf', 6, '1098466514_1673.pdf', '2020-06-02'),
(61, '1547/A-UIR/4-2020', 'Pemberitahuan', 'pdf', 6, '341436112_1547.pdf', '2020-05-06'),
(62, '1591/A-UIR/4-2020', 'Dokumen Akreditasi dan Validasi Data Mahasiswa Program Studi', 'jpg', 6, '1825930494_1591.jpg', '2020-05-18'),
(63, '1432/A-UIR/4-2020', 'Mekanisme Perpanjangan Akreditasi', 'pdf', 6, '516044381_1432.pdf', '2020-04-07'),
(64, '1433/A-UIR/4-2020', 'Pengisian Data Riwayat COVID-19', 'pdf', 6, '1590327351_1433.pdf', '2020-04-07'),
(65, '1337/A-UIR/4-2020', 'UTS dan Kegiatan Belajar Dalam Suasana COVID-19', 'jpg', 6, '53371072_1337.jpg', '2020-03-27'),
(66, '1295/A-UIR/4-2020', 'Izin Mengikuti Kegiatan di Luar Kampus Dalam Forum Lokal, Nasional Dan/Atau Internasional Terkait Pencegahan COVID-19 di Universitas Islam Riau', 'jpg', 6, '91299519_1295.jpg', '2020-03-23'),
(67, '1417/A-UIR/4-2020', 'Kegiatan Akademik Pada Masa Pandemi Wabah COVID-19', 'pdf', 6, '1845958370_1417.pdf', '2020-04-08'),
(68, '058/A-UIR/4-LPM/2020', 'Pemberitahuan Pelaksanaan Kegiatan Monitoring dan Evaluasi Tugas Akhir / Skripsi TA. 2019/2020 Universitas Islam Riau', 'pdf', 6, '509171572_058.pdf', '2020-02-11'),
(69, '1039/A-UIR/4-2020', 'Kewaspadaan Dini Terkait COVID-19', 'jpg', 6, '551611078_1039.jpg', '2020-03-04'),
(70, '0957/A-UIR/4-2020', 'Pemberitahuan Mengikuti Bimbingan Baca Al-Quran (BBQ) Bagi Mahasiswa Baru T.A 2019/2020', 'jpg', 6, '1667935559_0957.jpg', '2020-02-26'),
(71, '0656/A-UIR/4-2020', 'Pemberitahuan Penyampaian RKAT Tahun Anggaran 2019/2020', 'jpg', 6, '176944810_0656.jpg', '2020-02-10'),
(72, '0307/A-UIR/4-2020', 'Salinan Peraturan Rektor Universitas Islam Riau', 'pdf', 6, '655975917_0307.pdf', '2020-01-22'),
(73, '0356/A-UIR/4-2020', 'Libur Nasional dan Cuti Bersama Tahun 2020', 'pdf', 6, '1987649363_0356.pdf', '2020-01-23'),
(74, '0302/A-UIR/4-2020', 'Himbauan Libur Pada Acara Wisuda UIR', 'jpg', 6, '1530333423_0302.jpg', '2020-01-22'),
(75, '0134/A-UIR/4-2020', 'Himbauan Pengisian SKP Dosen PNS dpk UIR Tahun 2019', 'jpg', 6, '584601380_0134.jpg', '2020-01-10'),
(76, '0396/A-UIR/4-2020', 'Penerima Beasiswa Pemerintah Provinisi Riau Tahun Anggaran 2019 untuk Tahun Akademik 2019/2020 Universitas Islam Riau', 'pdf', 6, '1337340063_0396.pdf', '2020-01-27'),
(77, '0299/A-UIR/4-2020', 'Pemberitahuan', 'pdf', 6, '1602394828_0299.pdf', '2020-01-20'),
(78, '0195/a-UIR/4-2020', 'Pemberitahuan', 'pdf', 6, '325004889_0195.pdf', '2020-01-13'),
(79, '2218/A-UIR/5-2020', 'Surat Keterangan Aktif Kuliah Setelah masa langkau pada semester 3 dan 4', 'jpg', 7, '1686192192_2218.jpg', '2020-07-29'),
(80, '1558/B-UIR/8-2020', 'Pemberhentian Kontrak Kerja', 'jpg', 10, '1847821422_1558.jpg', '2020-05-11'),
(81, '960/B-UIR/8-FAI/2020', 'Permohonan Pindah Home base Dosen', 'pdf', 10, '711908912_960.pdf', '2020-05-06'),
(82, '2192/B-UIR/9-2020', 'Usulan Jabatan Fungsional Dosen Tetap', 'jpg', 11, '551613103_2192.jpg', '2020-07-28'),
(83, '0713/B-UIR/9-2020', 'Usulan Kenaikan Pangkat Dosen', 'jpg', 11, '1732216897_0713.jpg', '2020-02-13'),
(84, '364/B-UIR/10-FAI/2020', 'Laporan Absen Hadir Dosen', 'pdf', 12, '1142805492_364.pdf', '2020-03-02'),
(85, '1352/B-UIR/7-FAI/2020', 'Permohonan Dosen Luar Biasa', 'jpg', 9, '55531058_1352.jpg', '2020-07-22'),
(86, '1335/B-UIR/7-FAI/2020', 'Permohonan Penambahan Dosen', 'jpg', 9, '1986813377_1335.jpg', '2020-07-17'),
(87, '1210/B-UIR/11-FAI/2020', 'Usulan Sekretaris UPM ', 'jpg', 13, '2063404436_1210.jpg', '2020-07-01'),
(88, '1278/B-UIR/7-FAi/2020', 'Lamaran Dosen', 'jpg', 9, '1296121723_1278.jpg', '2020-07-09'),
(89, '921/B-UIR/7-FAI/2020', 'Nama dan Rencana Dosen FAI Untuk Melanjutkan Studi S.3', 'pdf', 9, '727318399_921.pdf', '2020-04-29'),
(90, '284/B-UIR/7-FAI/2020', 'Pindah Homebase', 'pdf', 9, '271959485_284.pdf', '2020-02-20'),
(91, '1025/B-UIR/7-FAI/2020', 'Usulan Ka. Prodi Fakultas Agama Islam Tahun 2020 s.d 2024', 'jpg', 9, '55442464_1025.jpg', '2020-05-28'),
(92, '1026/A-UIR/5-FAI/2020', 'Surat Pernyataan', 'pdf', 7, '1801607433_1026.pdf', '2020-05-28'),
(93, '933/B-UIR/7-FAI/2020', 'Nama dan Rencana Dosen FAI Untuk Melanjutkan Studi S.3', 'pdf', 9, '766797067_933.pdf', '2020-04-29'),
(94, '123/B-UIR/7-FAI/2020', 'Evaluasi Dosen Kontrak', 'jpg', 9, '1391381160_123.jpg', '2020-01-05'),
(95, '1039/C-UIR/14-FAI/2020', 'Permohonan ATK', 'jpg', 16, '1966687181_1039_C_UIR_$.jpg', '2020-06-03'),
(96, '1020/C-UIR/14-FAI/2020', 'Permohonan ATK', 'jpg', 16, '621189004_1020.jpg', '2020-05-27'),
(97, '350/C-UIR/14-FAI/2020', 'Permohonan ATK', 'jpg', 16, '905967485_350.jpg', '2020-03-02'),
(98, '02/C-UIR/14-FAI/2020', 'Permohonan ATK', 'jpg', 16, '888709332_02.jpg', '2020-01-02'),
(99, '133/C-UIR/14-FAI/2020', 'Mohon Pemindahan Papan Baleho dan Parkir Dosen', '', 16, '274002594_', '2020-02-06'),
(100, '024/C-UIR/14-FAI/2020', 'Mohon Perbaikan Ruangan Kuliah dan Perlengkapan', 'jpg', 16, '1494248841_024.jpg', '2020-01-09'),
(101, '1994/A-UIR/5-2020', 'Surat Keterangan Aktif Kuliah Setelah masa langkau pada semester 4', 'jpg', 7, '1927200993_1994.jpg', '2020-08-24'),
(102, '1333/C-UIR/16-FAI/2020', 'Laporan Penggunaan Dana Akreditasi Prodi PBA FAI UIR', 'jpg', 18, '1472977788_1333.jpg', '2020-07-14'),
(103, '1565/D-UIR/18-FAI/2020', 'Permohonan Pembukaan KRS Online', 'jpg', 20, '381641328_1565.jpg', '2020-08-19'),
(104, '1544/D-UIR/18', 'SK Tugas Belajar Lama (a.n) Amiruddin', 'pdf', 21, '1271374777_1544.pdf', '2020-08-18'),
(105, '1358/D-UIR/18', 'SK Tentang Dosen Pembina Semesetr Ganjil UIR TA 2020/2021', 'pdf', 20, '1521416819_1358.pdf', '2020-07-23'),
(106, '1100/D-UIR/18-FAI/2020', 'Data Mahasiswa Tidak Aktif FAI - UIR', 'pdf', 20, '1052372827_1100.pdf', '2020-06-11'),
(107, '1587/C-UIR/12-FAI/2020', 'Pencairan Dana KKN Tahap II', 'jpg', 14, '485013245_1587.jpg', '2020-08-24'),
(108, '1588/C-UIR/12-FAI/2020', 'Pencairan Dana Akreditasi PBSy Tahap III', 'jpg', 14, '1393730290_1588.jpg', '2020-08-24'),
(109, '1005/D-UIR/18-FAI/2020', 'Undangan Pelantikan & Sertijab', 'jpg', 20, '434718309_1005.jpg', '2020-05-28'),
(110, '671/D-UIR/18-FAI/2020', 'Pelaksanaan UTS Secara Daring', 'jpg', 20, '1058861807_671.jpg', '2020-03-23'),
(111, '372/D-UIR/18-FAI/2020', 'Pemberitahuan & Permohonan Naskah UTS', 'jpg', 20, '125244994_372.jpg', '2020-03-02'),
(112, '332/D-UIR/18-FAI/2020', 'Fasilitasi Entri Nilai ke SIKAD', 'jpg', 20, '1569520961_332.jpg', '2020-02-27'),
(113, '182/D-UIR/18-FAI/2020', 'Permohonan Menjadi Narasumber', 'jpg', 20, '291386470_182.jpg', '2020-02-11'),
(114, '77/D-UIR/18-FAI/2020', 'Jadwal Kuliah & Mohon Menyerahkan RPS & Kontrak Perkuliahan', 'jpg', 20, '718053946_77.jpg', '2020-01-27'),
(115, '26/D-UIR/18-FAI/2020', 'Pengantar Lembaran Jawaban UAS & Permintaan Nilai UAS Ganjil 2019/2020', 'jpg', 20, '1765361779_26.jpg', '2020-01-08'),
(116, '1560/D-UIR/19-2020', 'Rekomendasi / Izin Pindah', 'pdf', 21, '383578203_1560.pdf', '2020-05-08'),
(117, '0316/D-UIR/19-2020', 'Surat Keterangan Pindah Kuliah a.n Irfan Zul Amni', 'jpg', 21, '431338318_0316.jpg', '2020-01-23'),
(118, '0317/D-UIR/19-2020', 'Surat Keterangan Pindah Kuliah a.n Riki Rikardo', 'jpg', 21, '473566564_0317.jpg', '2020-01-23'),
(119, '943/D-UIR/20-FAI/2020', 'Mohon Pinjam Dosen Untuk S2 PAL', 'jpg', 22, '1472075755_943.jpg', '2020-05-06'),
(120, '498/D-UIR/20-FAI', 'Jemputan Sebagai Visiting Lecture', 'jpg', 22, '91776715_498.jpg', '2020-03-10'),
(121, '191/D-UIR-20/FAI-2020', 'Persetujuan Program Pensyarah Pelawat KUIPs', 'jpg', 22, '1982429352_191.jpg', '2020-02-25'),
(122, '172/D-UIR/20-FAI/2020', 'Jemputan Sebagai Visiting Lecture Dalam Program Kerjasama Antara Institusi Yaitu Universitas Islam Riau (UIR) dan Kolej Universiti Islam Perlis (KUIPs)', 'jpg', 22, '1143455307_172.jpg', '2020-02-11'),
(123, '134/D-UIR/20/FAI/2020', 'Visiting Lecture', 'jpg', 22, '1112972762_134.jpg', '2020-02-06'),
(124, '174/D-UIR/20-FAI/2020', 'Visiting Lecture', 'jpg', 22, '2145123539_174.jpg', '2020-02-11'),
(125, '116/D-UIR/20-FAI/2020', 'Visiting Lecture', 'jpg', 22, '2013746821_116.jpg', '2020-02-04'),
(126, '991/D-UIR/21-FAI/2020', 'Laporan Mahasiswa NPM 2013 Yang akan DO.', 'pdf', 23, '34823603_991.pdf', '2020-05-15'),
(127, '449/E-UIR/27-FAI/2020', 'Permohonan Menerima Mahasiswa KKN Fakultas Agama Islam, Universitas Islam Riau', 'jpg', 29, '141999925_449.jpg', '2020-03-09'),
(128, '935/E-UIR/28-FAI/2020', 'Undangan Rapat Senat', 'jpg', 30, '1555627387_935.jpg', '2020-05-08'),
(129, '1523/C-UIR/12-FAI/2020', 'Mohon Bantuan Dana', 'jpg', 14, '548164330_1523.jpg', '2020-08-13'),
(130, '959/C-UIR/12-FAI/2020', 'Permohonan Bon Nasi Piket Hari Kerja', 'jpg', 14, '1799907428_959.jpg', '2020-03-11'),
(131, '373/C-UIR/12-FAI/2020', 'Permohonan Bon Nasi Piket Hari Kerja', 'jpg', 14, '829655641_373.jpg', '2020-03-02'),
(132, '1443/C-UIR/12-FAI/2020', 'Pencairan Dana Akreditasi PBSy Tahap II', 'jpg', 14, '1023118022_1443.jpg', '2020-08-05'),
(133, '1421/C-UIR/12-FAI/2020', 'Pencairan Dana PPL dan Magang', 'pdf', 14, '1406702373_1421.pdf', '2020-07-20'),
(134, '1194/C-UIR/12-FAI/2020', 'Mohon Pencairan Dana KKN Tahap Kedua', 'jpg', 14, '457515095_1194.jpg', '2020-06-29'),
(135, '1160/C-UIR/12-FAI/2020', 'Laporan Pelaksanaan Ujian Akhir Semester Genap T.A 2019/2020', 'jpg', 14, '2023145571_1160.jpg', '2020-06-22'),
(136, '1036/C-UIR/12-FAI/2020', 'Permohonan Minuman', 'jpg', 14, '213282383_1036.jpg', '2020-06-03'),
(137, '961/C-UIR/12-FAI/2020', 'Mohon Pencairan Dana Penerbitan Jurnal Al-Hikmah ', 'jpg', 14, '1513465556_961.jpg', '2020-05-11'),
(138, '810/C-UIR/12-FAI/2020', 'Laporan Kegiatan di Luar Jam Kerja', 'jpg', 14, '884328139_810.jpg', '2020-03-31'),
(139, '433/C-UIR/12-FAI/2020', 'Mohon Bantuan Dana Sebagai Instruktur di UMKM Pemda Rohil', 'jpg', 14, '116160518_433.jpg', '2020-03-06'),
(140, '352/C-UIR/12-FAI/2020', 'Laporan Kegiatan di Luar Jam Kerja', 'jpg', 14, '1083490909_352.jpg', '2020-03-02'),
(141, '366/C-UIR/12-FAI/2020', 'Laporan Tatap Muka Dosen FAI Periode Bulan Februari 2020', 'jpg', 14, '1453028370_366.jpg', '2020-03-02'),
(142, '310/C-UIR/12-FAI/2020', 'Mohon Pencairan Dana Kuliah Umum', 'jpg', 14, '220334854_310.jpg', '2020-02-25'),
(143, '251/C-UIR/12-FAI/2020', 'Mohon Pencairan Dana Kuliah Umum', 'jpg', 14, '1286513586_251.jpg', '2020-02-18'),
(144, '208/C-UIR/12-FAI/2020', 'Mohon Pencairan Dana KKN', 'jpg', 14, '1589229047_208.jpg', '2020-02-14'),
(145, '179/C-UIR/12-FAI/2020', 'Mohon Pencairan Dana Taktis Tahap II', 'jpg', 14, '1092636397_179.jpg', '2020-02-12'),
(146, '100/E-UIR/28-FAI/2020', 'Penawaran Pengabdian Kepada Masyarakat Dosen Fakultas Agama Islam UIR', 'jpg', 14, '932178159_100.jpg', '2020-01-30'),
(147, '78/C-UIR/12-FAI/2020', 'Pencairan Dana CAV ISO', 'jpg', 14, '661782951_78.jpg', '2020-01-28'),
(148, '25/C-UIR/12-FAI/2020', 'Mohon Bantuan Dana MILAD FAI-UIR', 'jpg', 14, '227531397_25.jpg', '2020-01-09'),
(149, '35/C-UIR/12-FAI/2020', 'Mohon Pencairan Dana Promosi dan Sosialisasi', 'pdf', 14, '868795657_35.pdf', '2020-01-13'),
(150, '023/C-UIR/12-FAI/2020', 'Mohon Pencairan Dana Continual Asessment Visit (CAV) ISO 9001:2015', 'pdf', 14, '1260725751_023.pdf', '2020-01-09'),
(151, '12/C-UIR/12-FAI/2020', 'Pencairan Honor Panitia Kuliah Umum Prodi EKIS dan Prodi PBSY', 'jpg', 14, '1624251643_12.jpg', '2020-01-06'),
(152, '1193/C-UIR/12-FAI/2020', 'Mohon Pencairan Dana Operasional Periode Tahun Ajaran 2019/2020', 'jpg', 14, '1643222568_1193.jpg', '2020-06-29'),
(153, '1040/C-UIR/12-FAI/2020', 'Permohonan Bantuan Biaya Publikasi Ilmiah', 'jpg', 14, '1827799675_1040.jpg', '2020-06-03'),
(154, '994/C-UIR/12-FAI/2020', 'Permohonan Bantuan Biaya Publikasi Ilmiah', 'jpg', 14, '1640636079_994.jpg', '2020-05-18'),
(155, '931/C-UIR/12-FAI/2020', 'Permohonan Reward Dosen', 'jpg', 14, '28364668_931.jpg', '2020-05-04'),
(156, '573/C-UIR/12-FAI/2020', 'Pemberian Riward', 'jpg', 14, '436726027_573.jpg', '2020-03-13'),
(157, '431/C-UIR/12-FAI/2020', 'Mohon Bantuan Dana', 'jpg', 14, '616459838_431.jpg', '2020-03-05'),
(158, '377/C-UIR/2-FAI/2020', 'Permohonan Perbaikan Komputer', 'jpg', 14, '191174349_377.jpg', '2020-03-02'),
(159, '351/C-UIR/12-FAI/2020', 'Permohonan Minuman', 'jpg', 14, '1063383066_351.jpg', '2020-03-02'),
(160, '1334/C-UIR/12-FAI/2020', 'Mohon Pencairan Dana Matching Grant Prodi Ekonomi Syariah FAI - UIR', 'jpg', 14, '132537903_1334.jpg', '2020-07-15'),
(161, '1203/C-UIR/12-FAI/2020', 'Mohon Pencairan Dana Taktis Periode I', 'jpg', 14, '1379178920_1203.jpg', '2020-06-29'),
(162, '1247/C-UIR/12-FAI/2020', 'Mohon Pencairan Dana Akreditsi Tahap ke Dua', 'jpg', 14, '959849530_1247.jpg', '2020-07-06'),
(163, '1294/C-UIR/12-FAI/2020', 'Permintaan Konsumsi FAI', 'jpg', 14, '517687068_1294.jpg', '2020-07-13'),
(164, '01/C-UIR/12-FAI/2020', 'Laporan Kegiatan di Luar Jam Kerja', 'jpg', 14, '1725517922_01.jpg', '2020-01-02'),
(165, '03/C-UIR/12-FAI/2020', 'Permohonan Minuman', 'jpg', 14, '1687404906_03.jpg', '2020-01-02'),
(166, '1521/A-UIR/4-FAI/2020', 'Himbauan', 'jpg', 6, '572732086_1521.jpg', '2020-08-13'),
(167, '1479/A-UIR/3-FAI/2020', 'Rapat Rutin Bulanan', 'pdf', 5, '1039058300_1479.pdf', '2020-08-10'),
(168, '1445/A-UIR/3-FAI/2020', 'Rapat Akademik 2020/2021', 'jpg', 5, '1202148084_1445.jpg', '2020-08-05'),
(169, '1418/A-UIR/4-FAI/2020', 'Data Penerima Sertifikasi Dosen FAI', 'pdf', 6, '1251480544_1418.pdf', '2020-07-29'),
(170, '958/A-UIR/4-FAI/2020', 'Pengumuman', 'jpg', 6, '1535692088_958.jpg', '2020-05-08'),
(171, '996/A-UIR/4-FAI/2020', 'Pengumuman', 'jpg', 6, '387591467_996.jpg', '2020-05-14'),
(172, '1177/A-UIR/4-FAI/2020', 'Pemberitahuan Nonmenklatur', 'jpg', 6, '541048232_1177.jpg', '2020-06-26'),
(173, '1408/A-UIR/3-FAI/2020', 'Pertemuan dengan Kopertais Wilayah XII Riau Kepri', 'jpg', 5, '2089484081_1408.jpg', '2020-07-28'),
(174, '22/A-UIR/2-FAI/2020', 'LPJ Seminar Nasional Prodi EKIS dan Prodi PBSY', 'jpg', 4, '217472423_22.jpg', '2020-01-09'),
(175, '124/A-UIR/2-FAI/2020', 'Laporan Kegiatan Dosen', 'jpg', 4, '1858804014_124.jpg', '2020-02-05'),
(176, '153/A-UIR/2-FAI/2020', 'Laporan Pertanggung Jawaban Kunjungan Akademik FAI - UIR', 'jpg', 4, '1456177672_153.jpg', '2020-02-10'),
(177, '154/A-UIR/2-FAI/2020', 'Laporan Pertanggung Jawaban Kuliah Umum Prodi PIAUD', 'jpg', 4, '373146624_154.jpg', '2020-02-10'),
(178, '155/A-UIR/2-FAI/2020', 'Laporan Pelaksanaan Tugas Dekan Fakultas Agama Islam - UIR', 'jpg', 4, '1385989308_155.jpg', '2020-02-10'),
(179, '156/A-UIR/2-FAI/2020', 'Laporan Kegiatan Praktek Pengalaman Lapangan', 'jpg', 4, '426300664_156.jpg', '2020-02-10'),
(180, '518/A-UIR/2-FAI/2020', 'Rencana Kerja Tahunan (RKT) 2020 Fakultas Agama Islam - UIR', 'jpg', 4, '847211426_518.jpg', '2020-03-10'),
(181, '850/A-UIR/2-FAI/2020', 'SOP SEMPRO DAN KOMPRE SELAMA PANDEMI WABAH COVID-19', 'pdf', 4, '1263991488_850.pdf', '2020-04-09'),
(182, '876/A-UIR/2-FAI/2020', 'Laporan Hasil Visitasi ISO-9001-2015 Fakultas Agama Islam Tahun 2020', 'pdf', 4, '2057863009_876.pdf', '2020-04-16'),
(183, '903/A-UIR.2-FAI/2020', 'Laporan Progres Akreditasi 3 Prodi Fakultas Agama Islam - UIR', 'jpg', 4, '1588621049_903.jpg', '2020-04-27'),
(184, '998/A-UIR/2-FAI/2020', 'Laporan Pengunduran Diri Sekretaris UPM FAI', 'pdf', 4, '2026024654_998.pdf', '2020-05-19'),
(185, '1021/A-UIR/2-FAI/2020', 'Usulan Calon Wakil Dekan Fakultas Agama Islam', 'pdf', 4, '362572197_1021.pdf', '2020-05-27'),
(186, '0413/A-UIR/3-FAI/2020', 'Undangan', 'jpg', 5, '203861061_0413.jpg', '2020-01-30'),
(187, '1324/A-UIR/3-Dekan-FAI/2020', 'Pemberitahuan', 'jpg', 5, '1222681837_1324.jpg', '2020-07-16'),
(188, '1290/A-UIR/1-FAI/2020', 'Pemberitahuan Izin Tidak Masuk Kerja', 'jpg', 3, '1609724035_1290.jpg', '2020-03-04'),
(189, '1226/A-UIR/5-FAI/2020', 'Surat Tugas', 'jpg', 7, '922811146_1226.jpg', '2020-06-30'),
(190, '999/A-UIR/3-FAI/2020', 'Meyikapi Surat Rektor No : 1615/A-UIR/2-2020 Tentang Usulan Wakil Dekan', 'pdf', 5, '1478622901_999.pdf', '2020-05-26'),
(191, '1313/A-UIR/5-FAI/2020', 'Permohonan Rekomendasi Menjadi PTP KIP', 'jpg', 7, '171562433_1313.jpg', '2020-07-15'),
(192, '1254/A-UIR/1-FAI/2020', 'Permintaan Data Penelitian dan Pengabdian Masyarakat FAI Th. 2017 sd 2019', 'jpg', 3, '2005794038_1254.jpg', '2020-07-02'),
(193, '1257/A-UIR/1-FAI/2020', 'Mohon Memberikan Surat Pengantar/Rekomendasi Pengurusan Perubahan Nama Prodi', 'jpg', 3, '718184180_1257.jpg', '2020-06-07'),
(194, '1256/A-UIR/1-FAI/2020', 'Mohon Membuat Surat Perubahan Nama Prodi Ekonomi Islam Menjadi Ekonomi Syariah', 'jpg', 3, '667273296_1256.jpg', '2020-06-07'),
(195, '27/kpts/Dekan-FAI/2020', 'Tim Penyiapan Kompetisi Mahasiswa Fakultas Agama Islam', 'pdf', 40, '728724653_27_ktps_Dekan-FAI_2020.pdf', '2020-07-01'),
(196, '1296/A-UIR/3-FAI/2020', 'Permohonan Audiensi', 'jpg', 5, '581217909_1296.jpg', '2020-07-13'),
(197, '1220/A-UIR/3-FAI/2020', 'Undangan Rapat Koordinasi ', 'jpg', 5, '111235422_1220.jpg', '2020-07-01'),
(198, '1286/A-UIR/3-FAI/2020', 'Pemberitahuan', 'jpg', 5, '638039371_1286.jpg', '2020-07-09'),
(199, '1204/A-UIR/3-FAI/2020', 'Rapat Koordinasi Dan Pelantikan Daring', 'jpg', 5, '2085606743_1204.jpg', '2020-06-30'),
(200, '1287/A-UIR/3-FAI/2020', 'Pemberitahuan', 'jpg', 5, '1423806855_1287.jpg', '2020-07-09'),
(201, '1349/A-UIR/1-FAI/2020', 'Penilaian Dosen Personal Garansi', 'pdf', 3, '834709986_1349.pdf', '2020-07-20'),
(202, '2214/B-UIR/7-2020', 'Usulan SK Pengangkatan Untuk Penerbitan Nomor Induk Dosen Nasional/Khusus UIR', 'jpg', 9, '324047215_2214.jpg', '2020-07-20'),
(203, '0316/B-UIR/7-2020', 'Lamaran Sebagai Dosen AN Sawaluddin, S.PD.I., M.PD', 'jpg', 9, '1047674050_0316.jpg', '0020-12-11'),
(204, '87/B-UIR/7-2020', 'Peraturan Rektor Kerjasama Fakultas / Pascasarjana, Lembaga, Badan, Biro, Pusat Studi dan Pusat Kajian di Lingkungan UIR Dengan Pihak Ketiga', 'pdf', 9, '827806180_87.pdf', '2020-01-28'),
(205, '110/UIR/KPTS/2020', 'DOSEN PEMBINA SEMESTER GENAP UNIVERSITAS ISLAM RIAU T.A 2020/2021', 'pdf', 41, '68812916_110.pdf', '2020-02-10'),
(206, '108/UIR/KPTS/2020', 'PENGANGKATAN PROF DR. HJ. ELLYDAR CHAIDIR, S.H., M.HUM. SEBAGAI KETUA PROGRAM STUDI DOKTOR (S3) HUKUM PADA PROGRAM PASCASARJANA UNIVERSITAS ISLAM RIAU', 'pdf', 41, '1961633612_108.pdf', '2020-01-31'),
(207, '02/B-UIR/11-UPM-FAI/2020', 'Usulan Sekretaris UPM ', 'jpg', 13, '1143416701_02.jpg', '2020-06-30'),
(208, '1291/D-UIR/18-FAI/2020', 'Rasio dan Penambahan Dosen', 'jpg', 20, '959028687_1291.jpg', '2020-07-13'),
(209, '1717/A-UIR/3-2020', 'Undangan Diskusi Online', 'jpg', 5, '1462307908_1717.jpg', '2020-06-16'),
(210, '1725/A-UIR/3-2020', 'Undangan Sosialisasi Program MBKM', 'jpg', 5, '76734006_1725.jpg', '2020-06-16'),
(211, '1753/A-UIR/3-2020', 'Undangan Rapat Koordinasi ', 'jpg', 5, '29187009_1753.jpg', '2020-06-19'),
(212, '1161/A-UIR/3-FAI/2020', 'Mohon Membuka Acara Seminar Daring', 'jpg', 5, '568021956_1161.jpg', '2020-06-22'),
(213, '1069/A-UIR/3-FAI/2020', 'Permohonan sebagai Moderator Webinar', 'jpg', 5, '1867640585_1069.jpg', '2020-06-09'),
(214, '1668/A-UIR/3-2020', 'Undangan Rapat Koordinasi ', 'jpg', 5, '1797612681_1668.jpg', '2020-06-03'),
(215, '257/A-UIR/3-LPPM/2020', 'Undangan Seminar Online', 'jpg', 5, '1717620586_257.jpg', '2020-05-29'),
(216, '975/A-UIR/3-FAI/2020', 'Usulan Wakil Dekan FAI Dan Ka. Prodi', 'jpg', 5, '2088409078_975.jpg', '2020-05-11'),
(217, '1498/A-UIR/3-2020', 'Undangan Rapat', 'jpg', 5, '1553825683_1498.jpg', '2020-04-27'),
(218, '611/A-UIR/3-FAI/2020', 'Mohon Sebagai Pemateri', 'jpg', 5, '1959672678_611.jpg', '2020-03-16'),
(219, '1249/A-UIR/3-2020', 'Undangan Rapat', 'jpg', 5, '1043374352_1249.jpg', '2020-03-19'),
(220, '1613/A-UIR/3-2020', 'Undangan IKA FAI', 'pdf', 5, '1775675310_1613.pdf', '2020-08-26'),
(221, '558/A-UIR/3-2020', 'Undangan', 'jpg', 5, '552251971_558.jpg', '2020-03-12'),
(222, '400/A-UIR/3-PPs/2020', 'Undangan Menghadiri Kuliah Umum', 'jpg', 5, '404535013_400.jpg', '2020-03-09'),
(223, '192/A-UIR/3-LPPM/2020', 'Undangan', 'pdf', 5, '1843696918_192.pdf', '2020-03-07'),
(224, '182/A-UIR/3-LPPM/2020', 'Undangan', 'jpg', 5, '939418205_182.jpg', '2020-03-02'),
(225, '404/A-UIR/3-FAI/2020', 'Undangan Silaturahmi Dan Dialog', 'jpg', 5, '1133108194_404.jpg', '2020-03-04'),
(226, '0959/A-UIR/3-2020', 'Undangan', 'jpg', 5, '170467784_0959.jpg', '2020-03-03'),
(227, '338/A-UIR/3-FAI/2020', 'Undangan Pemateri Kuliah Umum', 'jpg', 5, '57252914_338.jpg', '2020-02-20'),
(228, '0893/A-UIR/3-2020', 'Undangan Kompetisi Debat Konstituisi Mahasiswa Se-Indonesia XIII', 'jpg', 5, '1692616858_0893.jpg', '2020-02-22'),
(229, '0857/A-UIR/3-2020', 'Undangan', 'jpg', 5, '1827628435_0857.jpg', '2020-02-24'),
(230, '0930/A-UIR/3-2020', 'Undangan', 'jpg', 5, '11850086_0930.jpg', '2020-02-26'),
(231, '0796/A-UIR/3-2020', 'Undangan Jalan Santai', 'jpg', 5, '128307907_0796.jpg', '2020-02-18'),
(232, '229/A-UIR/3-FAI/2020', 'Undangan Silaturahmi', 'jpg', 5, '240576472_229.jpg', '2020-02-15'),
(233, '228/A-UIR/3-FAI/2020', 'Undangan Rapat', 'jpg', 5, '103358553_228.jpg', '2020-02-15'),
(234, '0655/A-UIR/3-FAI/2020', 'Undangan', 'jpg', 5, '1309136795_0655.jpg', '2020-02-12'),
(235, '0650/A-UIR/3--2020', 'Undangan', 'jpg', 5, '699983318_0650.jpg', '2020-02-12'),
(236, '143/A-UIR/3-FAI/2020', 'Permohonan Menjadi Narasumber Kuliah Umum', 'jpg', 5, '220457064_143.jpg', '2020-02-07'),
(237, '131/A-UIR/3-FAI/2020', 'Undangan Silaturahmi', 'jpg', 5, '77497697_131.jpg', '2020-02-05'),
(238, '152/A-UIR/3-FAI/2020', 'Pendampingan Persiapan Visitasi ISO', 'jpg', 5, '2100558176_152.jpg', '2020-02-10'),
(239, '118/A-UIR/3-FAI/2020', 'Undangan Rapat Pemilihan Dekan', 'pdf', 5, '1221390943_118.pdf', '2020-02-05'),
(240, '150/A-UIR/3-FAI/2020', 'Undangan Rapat Pemilihan Dekan', 'jpg', 5, '1756031978_150.jpg', '2020-02-08'),
(241, '46/A-UIR/3-FAI/2020', 'Undangan', 'jpg', 5, '304377260_46.jpg', '2020-01-15'),
(242, '79/A-UIR/3-FAI/2020', 'Undangan Rapat', 'jpg', 5, '1172723382_79.jpg', '2020-01-28'),
(243, '0397/A-UIR/3-FAI/2020', 'Utusan Mahasiswa', 'jpg', 5, '897329701_0397.jpg', '2020-01-27'),
(244, '42/A-UIR/3', 'Undangan', 'jpg', 5, '225380620_42.jpg', '2020-01-14'),
(245, '1173/A-UIR/5-FAI/2020', 'Surat Tugas', 'jpg', 7, '1533345354_1173.jpg', '2020-06-25'),
(246, '1172/A-UIR/5', 'Permohonan Izin dan Surat Tugas', 'pdf', 7, '120845192_1172.pdf', '2020-06-25'),
(247, '1074/A-UIR/5-2020', 'Surat Keterangan Masa Langkau an Mauli Quratuain Rafilah', 'jpg', 7, '2015608449_1074.jpg', '2020-03-07'),
(248, '568/A-UIR/5-FAI/2020', 'Surat Keterangan', 'jpg', 7, '1093929735_568.jpg', '2020-03-13'),
(249, '542/A-UIR/5-FAI/2020', 'Daftar Nama Mahasiswa FAI NPM 2013', 'pdf', 7, '389816163_542.pdf', '2020-02-12'),
(250, '497/A-UIR/5-FAI/2020', 'Surat Tugas', 'jpg', 7, '1759977088_497.jpg', '2020-03-10'),
(251, '424/A-UIR/5-FAI/2020', 'Surat Kuasa', 'jpg', 7, '748372920_424.jpg', '2020-03-05'),
(252, '227/A-UIR/5-FAI/2020', 'Surat Tugas', 'jpg', 7, '1690377007_227.jpg', '2020-02-15'),
(253, '171/A-UIR/5-FAI/2020', 'Surat Tugas', 'jpg', 7, '1899450868_171.jpg', '2020-02-11'),
(254, 'Arsip Rapat', 'Rapat Akreditasi 19 Juni 2020', 'pdf', 42, '222311530_rapat akreditasi.pdf', '2020-06-19'),
(255, 'Arsip Rapat', 'Serah Terima Jabatan Ka. Prodi dan Sekretaris Prodi di Lingkungan FAI-UIR', '', 42, '801186481_', '2020-06-02'),
(256, 'Arsip Rapat', 'Sosialisasi Sistem Online Fakultas Agama Islam', 'pdf', 42, '1179030329_15 mei 2020.pdf', '2020-05-15'),
(257, 'Arsip Rapat', 'Rapat Koordinasi Perdana Dekan FAI Periode 2020-2024', 'pdf', 42, '1735060062_7 april 2020.pdf', '2020-04-07'),
(258, 'Arsip Rapat', 'Rapat Pleno RAB', 'pdf', 42, '1037372858_8 mei 2020.pdf', '2020-05-08'),
(259, '92/kpts/Dekan-FAI/2020', 'Surat Keputusan Dekan Fakultas Agama Islam Tentang Penetapan Dosen Pembimbing Penulisan Skripsi Mahasiswa Fakultas Agama Islam Universitas Islam Riau Pekanbaru', 'jpg', 40, '1441913915_92.jpg', '2020-04-20'),
(260, '24/UIR_FAI/kpts/2020', 'Surat Keputusan Dekan Fakultas Agama Islam Tentang Tim Penyusun Lembaran-lembaran Kerja Program Studi (LKPS) Prodi Perbankan Syariah Fakultas Agama Islam UIR', 'pdf', 40, '2050241312_24.pdf', '2020-06-22'),
(261, '21/UIR-FAI/Kpts/2020', 'Surat Keputusan Dekan Fakultas Agama Islam Tentang Tim Penyusun Borang Lembaran Evaluasi Diri (LED) Prodi Pendidikan Bahasa Arab (PBA) Fakultas Agama Islam UIR', 'pdf', 40, '1371880760_21.pdf', '2020-06-22'),
(262, '22/UIR-FAI/Kpts/2020', 'Surat Keputusan Dekan Fakultas Agama Islam Tentang Tim Penyusunan Lembaran Lembaran Kerja Program Studi (LKPS) Prodi Pendidikan Bahasa Arab Fakultas Agama Islam UIR', 'pdf', 40, '1573121556_22.pdf', '2020-06-22'),
(263, '23/UIR/FAI/Kpts/2020', 'Surat Keputusan Dekan Fakultas Agama Islam Tentang Tim Penyusun Borang Lembaran Evaluasi Diri (LED) Prodi Perbankan Syariah Fakultas Agama Islam UIR', 'pdf', 40, '498127696_23.pdf', '2020-06-22'),
(264, '17/Kpts/Dekan-FAI/2020', 'Surat Keputusan Dekan Fakultas Agama Islam Tentang Tim Seleksi Setoran Hafalan Al-Quran Juz 30 dan Hadis Pendidikan Prodi Pendidikan Islam Anak Usia Dini (PIAUD) Fakultas Agama Islam Angkatan 2018/2019 UIR', 'pdf', 40, '914187004_17.pdf', '2020-07-01'),
(265, '01/Kpts/Dekan-FAI/2020', 'Surat Keputusan Dekan Fakultas Agama Islam Tentang Panitian Milad Fakultas Agama Islam UIR', 'pdf', 40, '815608627_01.pdf', '2020-01-06'),
(266, '26/Kpts/Dekan-FAI/2020', 'Surat Keputusan Dekan Fakultas Agama Islam Tentang pembimbing Program Kemahasiswa Fakultas Agama Islam', 'pdf', 40, '1450889104_26.pdf', '2020-07-01'),
(267, '18/Kpts/Dekan-FAI/2020', 'Surat Keputusan Dekan Fakultas Agama Islam Tentang Susunan Pengurus Forum Studi Islam (FSI) Al-Ishlah Fakultas Agama Islam UIR Periode 2020-2021', '', 40, '1241956092_', '2020-07-01'),
(268, '029/Kpts/FAI-UIR/2020', 'Surat Keputusan Dekan Fakultas Agama Islam Tentang Pengelola Al-Hikmah : Jurnal Agama Dan Ilmu Pengetahuan Periode 2020-2024', 'pdf', 40, '545933672_029.pdf', '2020-07-01'),
(269, '06/Kpts/Dekan-FAI/2020', 'Surat Keputusan Dekan Fakultas Agama Islam Tentang Ancaman Skorsing Dan Pemberian Sanksi Teguran Keras kepada Mahasiswa Fakultas Agama Islam Yang Melakukan Pemalsuan Tandatangan', 'jpg', 40, '384060657_06.jpg', '2020-03-24'),
(270, '20/Kpts/Dekan-FAI/2020', 'Surat Keputusan Dekan Fakultas Agama Islam Tentang Manager BMT (Baitul Mal Tanwil) Fakultas Agama Islam UIR', 'jpg', 40, '1864058979_20.jpg', '2020-07-01'),
(271, '65/Kpts/Dekan-FaI/2020', 'Surat Keputusan Dekan Fakultas Agama Islam Tentang Ancaman Skorsing Dan Pemberian Sanksi Teguran Keras kepada Mahasiswa Fakultas Agama Islam Yang Melakukan Pemalsuan Tandatangan', 'jpg', 40, '1383462004_65.jpg', '2020-03-24'),
(272, '03/Kpts/Dekan-FAI/2020', 'Surat Keputusan Dekan Fakultas Agama Islam Tentang Pembentukan Pengurus Dewan Mahasiswa (DEMA) Fakultas Agama Islam UIR Periode 2020/2021', 'pdf', 40, '1471518118_03.pdf', '2020-02-18'),
(273, '13/Ktps/Dekan-FAI/2020', 'Surat Keputusan Dekan Fakultas Agama Islam Tentang Susunan Kepungurusan Koperasi Syariah Darusallam Periode 2020-2024 Fakultas Agama Islam UIR', 'pdf', 40, '612457994_13.pdf', '2020-05-14'),
(274, '15/Kpts/Dekan-FAI/2020', 'Surat Keputusan Dekan Fakultas Agama Islam Tentang Pengurus Cellad Fakultas Agama Islam Universitas Islam Riau Periode 2020-2024', 'pdf', 40, '1742609210_15.pdf', '2020-05-30'),
(275, '09/Kpts/Dekan-FAI/2020', 'Surat Keputusan Dekan Fakultas Agama Islam Tentang Pengangkatan Kepala Labor Prodi PIAUD Fakultas Agama Islam Universitas Islam Riau Pekanbaru', 'jpg', 40, '1082293911_09.jpg', '2020-05-13'),
(276, '10/Kpts/Dekan-FAI/2020', 'Surat Keputusan Dekan Fakultas Agama Islam Tentang Pengangkatan Kepala Labor Statistik Fakultas Agama Islam Universitas Islam Riau Pekanbaru', 'jpg', 40, '1905628004_10.jpg', '2020-05-13'),
(277, '11/Kpts/Dekan-FAI/2020', 'Surat Keputusan Dekan Fakultas Agama Islam Tentang Pengangkatan Kepala Labor Mikro Teaching Fakultas Agama Islam Universitas Islam Riau Pekanbaru', 'jpg', 40, '514285882_11.jpg', '2020-05-13'),
(278, '08/Kpts/Dekan-FAI/2020', 'Surat Keputusan Dekan Fakultas Agama Islam Tentang Pengangkatan Kepala Labor Bank Mini Syariah Fakultas Agama Islam Universitas Islam Riau Pekanbaru', 'jpg', 40, '191108425_08.jpg', '2020-05-13');

-- --------------------------------------------------------

--
-- Table structure for table `kategori`
--

CREATE TABLE `kategori` (
  `id` int(11) NOT NULL,
  `nama_kategori` varchar(255) DEFAULT NULL,
  `kode_kategori` varchar(255) DEFAULT NULL,
  `keterangan_kategori` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kategori`
--

INSERT INTO `kategori` (`id`, `nama_kategori`, `kode_kategori`, `keterangan_kategori`) VALUES
(3, 'Administrasi / Surat Umum', 'A-UIR/1', 'Umum'),
(4, 'Statistik / Dokumentasi / Laporan Data', 'A-UIR/2', 'Umum'),
(5, 'Undangan / Berita Acara / Perslagh Rapat', 'A-UIR/3', 'Umum'),
(6, 'Pengumuman / Radiogram / Telegram / Himbauan', 'A-UIR/4', 'Umum'),
(7, 'Surat Keterangan / Kuasa / Dispensasi / Rekomendasi / Surat Tugas / SPPD', 'A-UIR/5', 'Umum'),
(8, 'Surat Hansip / Menwa / Satpam', 'A-UIR/6', 'Umum'),
(9, 'Permohonan / Pengangkatan / Pemindahan / Mutasi', 'B-UIR/7', 'Personalia / Kepegawaian'),
(10, 'Pemberhentian / Skorsing / Pembubaran ', 'B-UIR/8', 'Personalia / Kepegawaian'),
(11, 'Kenaikan Pangkat / Cuti / Istirahat / Izin', 'B-UIR/9', 'Personalia / Kepegawaian'),
(12, 'Panitia / Seksi / Lembaga / Komisi / Team', 'B-UIR/10', 'Personalia / Kepegawaian'),
(13, 'Dan lain-lain yang belum termasuk Kode (B) yang berhubungan Dengan personalia', 'B-UIR/11', 'Personalia / Kepegawaian'),
(14, 'Rencana Anggaran Biaya / Pembelian Alat-alat', 'C-UIR/12', 'Keuangan'),
(15, 'Bantuan / Darma / Wakaf / Zakat', 'C-UIR/13', 'Keuangan'),
(16, 'Inventaris / Pemeliharaan', 'C-UIR/14', 'Keuangan'),
(17, 'Pembangunan / Pemeliharaan Gudang', 'C-UIR/15', 'Keuangan'),
(18, 'Laporan Keuangan / S P J', 'C-UIR/16', 'Keuangan'),
(19, 'Lain-lain yang belum termasuk kode (c)', 'C-UIR/17', 'Keuangan'),
(20, 'Kurikulum / Daftar kuliah / Praktikum', 'D-UIR/18', 'Pendidikan'),
(21, 'Penerimaan Mahasiswa / Berhenti / Pindah', 'D-UIR/19', 'Pendidikan'),
(22, 'OSPEK / ORDIKNAS / Kegiatan Mahasiswa / Kegiatan Dosen', 'D-UIR/20', 'Pendidikan'),
(23, 'Testing / Mid Semester / Ujian Semester', 'D-UIR/21', 'Pendidikan'),
(24, 'Ujian Negara / Pengawasan TK. SML/ D2, D3', 'D-UIR/22', 'Pendidikan'),
(25, 'Ujian Negara / Pengawasan TK. Strata Satu / S1', 'D-UIR/23', 'Pendidikan'),
(26, 'Seminar / Simposium / Penataan / Kursus / Raker / Munas', 'D-UIR/24', 'Pendidikan'),
(27, 'Permintaan / Pemesanan Buku / Majalah / Brosur Koran', 'E-UIR/25', 'Perpustakaan / Penelitian / KKN'),
(28, 'Pengiriman Buku / Majalah / Brosur / Koran', 'E-UIR/26', 'Perpustakaan / Penelitian / KKN'),
(29, 'Surat Umum Mengenai Penelitian KKN', 'E-UIR/27', 'Perpustakaan / Penelitian / KKN'),
(30, 'Anggaran Biaya Penelitian KKN', 'E-UIR/28', 'Perpustakaan / Penelitian / KKN'),
(31, 'Laporan Hasil Penelitian KKN', 'E-UIR/29', 'Perpustakaan / Penelitian / KKN'),
(39, 'Lain-lain yang belum termasuk kode (d)', 'E-UIR/30', 'Perpustakaan / Penelitian / KKN'),
(40, 'Surat Keputusan Dekan', 'SK Dekan', 'SK Dekan'),
(41, 'Surat Keputusan Rektor', 'SK Rektor', 'SK Rektor'),
(42, 'Dan Lain-lain', 'LL', '-');

-- --------------------------------------------------------

--
-- Table structure for table `riwayat_unduh`
--

CREATE TABLE `riwayat_unduh` (
  `id_riwayat` int(11) NOT NULL,
  `waktu` datetime DEFAULT NULL,
  `id_pengguna` int(11) DEFAULT NULL,
  `id_arsip` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `nama` varchar(255) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `level` varchar(255) DEFAULT NULL,
  `foto` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `nama`, `username`, `password`, `level`, `foto`) VALUES
(3, 'Admin FAI', 'admin_fai', '202cb962ac59075b964b07152d234b70', 'admin', '2122929585_Universitas_islam_riau_logo.jpg'),
(11, 'Dekan FAI', 'dekan_fai', '202cb962ac59075b964b07152d234b70', 'pengguna', '1852959131_Universitas_islam_riau_logo.jpg'),
(12, 'Wakil Dekan 1', 'wd1_fai', '202cb962ac59075b964b07152d234b70', 'pengguna', '1021464402_Universitas_islam_riau_logo.jpg'),
(13, 'Wakil Dekan 2', 'wd2_fai', '202cb962ac59075b964b07152d234b70', 'pengguna', '216958244_Universitas_islam_riau_logo.jpg'),
(14, 'Wakil Dekan 3', 'wd3_fai', '202cb962ac59075b964b07152d234b70', 'pengguna', '619432403_Universitas_islam_riau_logo.jpg'),
(15, 'Kepala TU FAI', 'ka_tu_fai', '202cb962ac59075b964b07152d234b70', 'pengguna', '112249792_Universitas_islam_riau_logo.jpg');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `data_arsip`
--
ALTER TABLE `data_arsip`
  ADD PRIMARY KEY (`id_arsip`),
  ADD KEY `kategori_arsip` (`kategori_arsip`);

--
-- Indexes for table `kategori`
--
ALTER TABLE `kategori`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `riwayat_unduh`
--
ALTER TABLE `riwayat_unduh`
  ADD PRIMARY KEY (`id_riwayat`),
  ADD KEY `id_pengguna` (`id_pengguna`),
  ADD KEY `id_arsip` (`id_arsip`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `data_arsip`
--
ALTER TABLE `data_arsip`
  MODIFY `id_arsip` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=279;

--
-- AUTO_INCREMENT for table `kategori`
--
ALTER TABLE `kategori`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;

--
-- AUTO_INCREMENT for table `riwayat_unduh`
--
ALTER TABLE `riwayat_unduh`
  MODIFY `id_riwayat` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `data_arsip`
--
ALTER TABLE `data_arsip`
  ADD CONSTRAINT `data_arsip_ibfk_1` FOREIGN KEY (`kategori_arsip`) REFERENCES `kategori` (`id`);

--
-- Constraints for table `riwayat_unduh`
--
ALTER TABLE `riwayat_unduh`
  ADD CONSTRAINT `riwayat_unduh_ibfk_1` FOREIGN KEY (`id_pengguna`) REFERENCES `user` (`id`),
  ADD CONSTRAINT `riwayat_unduh_ibfk_2` FOREIGN KEY (`id_arsip`) REFERENCES `data_arsip` (`id_arsip`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
