<?php include 'header.php' ?>
<body class="bg-primary">
	<div class="container">

		<div class="card o-hidden border-0 shadow-lg my-5 col-lg-5 mx-auto">
			<div class="card-body p-0">
				<div class="row">
					<div class="col-lg">
						<div class="p-5">
							<div class="text-center">
								<h1 class="h4 text-gray-900 mb-4">Silahkan Login</h1>
							</div>
							<form action="cek_login.php" method="POST">

								<div class="form-group">
									<label for="username">Username</label>
									<input class="form-control" type="text" name="username" placeholder="Username" autocomplete="off">
								</div>

								<div class="form-group">
									<label for="password">Password</label>
									<input class="form-control" type="password" name="password" placeholder="Password" autocomplete="off">
								</div>

								<p align="center"><input type="submit" class="btn btn-success btn-block" name="login" value="Masuk"></a></p>
								
							</form>

						</div>
					</div>
				</div>
<?php include 'footer.php' ?>
